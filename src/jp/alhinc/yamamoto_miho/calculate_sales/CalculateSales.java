package jp.alhinc.yamamoto_miho.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {

		HashMap<String, String>branchName = new HashMap<String, String>();

		HashMap<String, Long>saleMap = new HashMap<String, Long>();

		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		if (!reader("支店定義", "^[0-9]{3}$", args[0], "branch.lst", branchName, saleMap)) {
			return;
		}

		File dir = new File(args[0]);
		File[] list = dir.listFiles();

		ArrayList<File> fileName = new ArrayList<File>();

		for(int i = 0; i < list.length; i++) {

			if(list[i].isFile() && list[i].getName().matches("^[0-9]{8}.rcd$")) {
				fileName.add(list[i]);
			}
		}

		for(int m = 0; m < fileName.size() -1; m++) {

			String fileNum  = fileName.get(m).getName().substring(0,8);
			String fileNum2 = fileName.get(m + 1).getName().substring(0,8);

			int num = Integer.parseInt(fileNum);
			int num2 = Integer.parseInt(fileNum2);

			if(num2 - num != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		BufferedReader sr = null;

		for(int j=0; j < fileName.size(); j++) {

			try {
				ArrayList<String> branchList = new ArrayList<String>();

				File file1 = new File(args[0],fileName.get(j).getName());
				FileReader bf = new FileReader(file1);
				sr = new BufferedReader(bf);

				String line2;
				while((line2 = sr.readLine()) != null){
					branchList.add(line2);
				}

				if(branchList.size() != 2){
					System.out.println(fileName.get(j).getName() + "のフォーマットが不正です");
					return;
				}

				if(!branchList.get(1).matches("[0-9]*")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				if(!branchName.containsKey(branchList.get(0))) {
					System.out.println(fileName.get(j).getName() + "の支店コードが不正です");
					return;
				}

				long sale = Long.parseLong(branchList.get(1));

				saleMap.put(branchList.get(0), (saleMap.get(branchList.get(0)) + sale));

				if(saleMap.get(branchList.get(0)) > 9999999999L){
					System.out.println("合計金額が10桁を超えました");
					return;
				}

			}catch(IOException e){
				System.out.print("予期せぬエラーが発生しました");
				return;
			}finally {
				if(sr != null) {
					try {
						sr.close();
					}catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}
		if (!writer(args[0], "branch.out", branchName, saleMap)) {
			return ;
		}
	}

	public static boolean writer(String path, String branchFile, Map<String, String>branchNames, Map<String, Long>branchSales){

		BufferedWriter bw1 = null;

		try {
			File file = new File(path , branchFile);
			FileWriter fw = new FileWriter(file);
			bw1 = new BufferedWriter(fw);

			for(String key : branchNames.keySet()) {
				bw1.write(key + "," +branchNames.get(key) + "," + branchSales.get(key)) ;
				bw1.newLine();
			}
		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			if(bw1 != null) {
				try {
					bw1.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	public static boolean reader(String name, String checkCode, String path, String branchFiles, Map<String, String>nameMap, Map<String, Long>branchSale) {

		BufferedReader br = null;

		try {
			File file = new File(path, branchFiles);

			if(!file.exists()) {
				System.out.println(name + "ファイルが存在しません");
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			while((line = br.readLine()) != null) {

				String[] branch = line.split(",");

				if(branch.length != 2 || !branch[0].matches(checkCode)) {
					System.out.println(name + "ファイルのフォーマットが不正です");
					return false;
				}

				nameMap.put(branch[0] , branch[1]);
				branchSale.put(branch[0],(long) 0);

			}
		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			if(br != null) {
				try {
					br.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

}